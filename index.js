// console.log("Hi Maam Mia");

//Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
let getCube = (number) => console.log(`The cube of ${number} is ${number**3}`)

getCube(2);


//Create a variable address with a value of an array containing details of an address.


let address = ["258 Washington Ave NW", "California 90011"];
let [avenue, city] = address;

console.log(`I live at ${avenue}, ${city}`);


//Create a variable animal with a value of an object data type with different animal details as it’s properties.

let animal = {
	type: "saltwater crocodile",
	name: "Lolong",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {type, name, weight, measurement} = animal;

console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}`);


//Create an array of numbers.

let numbers = [1,2,3,4,5];


//Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numbers.forEach((number)=>console.log(number))


//Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

let newDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(newDog);

